Activitat 1. REALITZA I/O RESPON ELS SEGÜENTS APARTATS (obligatòria) (1 punts)
•Indica quins són els motors d’emmagatzematge que pots utilitzar (quins estan actius)?
Mostra al comanda utilitzada i el resultat d’aquesta.
•Com puc saber quin és el motor d’emmagatzematge per defecte. Mostra com canviar aquest
paràmetre de tal manera que les noves taules que creem a la BD per defecte utilitzin el motor
MyISAM?
•Com podem saber quin és el motor d'emmagatzematge per defecte?
•Explica els passos per instal·lar i activar l'ENGINE MyRocks. MyRocks és un motor
d'emmagatzematge per MySQL basat en RocksDB (SGBD incrustat de tipus clau-valor).
Aquest tipus d’emmagatzematge està optimitzat per ser molt eficient en les escriptures amb
lectures acceptables.
Checkpoint: Mostra al professor que està instal•lat i posa un exemple de com funciona.
•Importa la BD Sakila com a taules MyISAM. Fes els canvis necessaris per importar la BD
Sakila perquè totes les taules siguin de tipus MyISAM.
Mira quins són els fitxers físics que ha creat, quan ocupen i quines són les seves extensions.
Mostra'n una captura de pantalla i indica què conté cada fitxer.
Un cop fet això torna a deixar el motor InnoDB per defecte.
•A partir de MySQL apareixen els schemas de metadades i informació guardats amb InnoDB.
Busca informació d'aquests schemas. Indica quin és l'objectiu de cadascun d'ells i posa'n un
exemple d'ús.
•Posa un exemple que produeix un DEADLOCK i mostra-ho al professor.
Activitat 2. INNODB part I. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (2 punts)
•Desactiva l’opció que ve per defecte de innodb_file_per_table
•Importa la BD Sakila com a taules InnoDB.
•Quin/quins són els fitxers de dades? A on es troben i quin és la seva mida?
•Canvia la configuració del MySQL per:
•Canviar la localització dels fitxers del tablespace de sistema per defecte a /discs-mysql/
•Tinguem dos fitxers corresponents al tablespace de sistema.
•Tots dos han de tenir la mateixa mida inicial (10MB)
•El tablespace ha de créixer de 5MB en 5MB.
•Situa aquests fitxers (de manera relativa a la localització per defecte) en una nova
localització simulant el següent:
•/discs-mysql/disk1/primer fitxer de dades → simularà un disc dur
•/discs-mysql/disk2/segon fitxer de dades → simularà un segon disc dur.
•(0,5 punts) si les dues rutes anteriors són dos discs físics diferents.
Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant.
Activitat 3. INNODB part II. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (1 punt)
•Partint de l'esquema anterior configura el Percona Server perquè cada taula generi el seu
propi tablespace en una carpeta anomenada tspaces (aquesta pot estar situada a on
vulgueu).
•Indica quins són els canvis de configuració que has realitzat.
Activitat 4. INNODB part III. REALITZA ELS SEGÜENTS APARTATS (obligatòria) (1 punt)
•Crea un tablespace anomenat 'ts1' situat a /discs-mysql/disc1/ i col•loca les taules actor,
address i category de la BD Sakila.
•Crea un altre tablespace anomenat 'ts2' situat a /discs-mysql/disc2/ i col•loca-hi la resta de
taules.
•Comprova que pots realitzar operacions DML a les taules dels dos tablespaces.
•Quines comandes i configuracions has realitzat per fer els dos apartats anteriors?
Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant
Activitat 5. REDOLOG. REALITZA ELS SEGÜENTS APARTATS. (2 punt)
•Com podem comprovar (Innodb Log Checkpointing):
•LSN (Log Sequence Number)
•L'últim LSN actualitzat a disc
•Quin és l'últim LSN que se li ha fet Checkpoint
•Proposa un exemple a on es vegi l'ús del redolog
•Com podem mirar el número de pàgines modificades (dirty pages)? I el número total de
pàgines?
Checkpoint: Mostra al professor els canvis realitzats i que la BD continua funcionant.
Activitat 6. Implentar BD Distribuïdes. (1,5 punts)
Com s'ha vist a classe MySQL proporciona el motor d'emmagatzemament FEDERATED que té com
a funció permetre l'accés remot a bases de dades MySQL en un servidor local sense utilitzar
tècniques de replicació ni clustering.
•Prepara un Servidor Percona Server amb la BD de Sakila
•Prepara un segon servidor Percona Server a on hi hauran un conjunt de taules
FEDERADES al primer servdor.
•Per realitzar aquest link entre les dues BD podem fer-ho de dues maneres:
•Opció1: especificar TOTA la cadena de connexió a CONNECTION
•Opció2: especifficar una connexió a un server a CONNECTION que prèviament s'ha creat
mitjançant CREATE SERVER
•Posa un exemple de 2 taules de cada opció.
Tingues en compte els permisos a nivell de BD i de SO així com temes de seguretat com
firewalls, etc...
•Detalla quines són els passos i comandes que has hagut de realitzar en cada màquina.
funcionament.
Activitat 7. Storage Engine CSV (0,5 punts)
•Documenta i posa exemple de com utilitzar ENGINE CSV.
•Cal documentar els passos que has hagut de realitzar per preparar l'exemple:
configuracions, instruccions DML, DDL, etc....
funcionament.
Activitat 8. Storage Engine MyRocks (1 punt)
•Documenta i posa exemple de com utilitzar ENGINE MyRocks. Crea una Base de dades
amb 2 o 3 taules i inserta-hi contingut.
•Cal documentar els passos que has hagut de realitzar per preparar l'exemple:
configuracions, instruccions DML, DDL, etc....
•A quin directori es guarden els fitxers de dades? Fes un llistat de a on són els fitxers i què
ocupen.
•Quina és la compressió per defecte que utilitza per les taules? Com ho faries per canviarlo. Per exemple utilitza Zlib o ZSTD o sense compressió.
